import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Track, TrackDocument } from '../schemas/track.schema';
import { Model } from 'mongoose';
import { CreateTrackDto } from './create-track.dto';

@Controller('tracks')
export class TracksController {
  constructor(
    @InjectModel(Track.name)
    private trackModel: Model<TrackDocument>,
  ) {}

  @Get()
  getAllTracks(@Query() query: { albumId: string }) {
    return this.trackModel.find(query);
  }

  @Post()
  createTrack(@Body() trackDto: CreateTrackDto) {
    const track = new this.trackModel({
      name: trackDto.name,
      album: trackDto.album,
      duration: trackDto.duration,
    });

    return track.save();
  }

  @Delete(':id')
  deleteTrack(@Param('id') id: string) {
    return this.trackModel.deleteOne({ _id: id });
  }
}
