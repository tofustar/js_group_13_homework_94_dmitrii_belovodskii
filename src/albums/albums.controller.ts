import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { Album, AlbumDocument } from '../schemas/album.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateAlbumDto } from './create-album.dto';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('albums')
export class AlbumsController {
  constructor(
    @InjectModel(Album.name)
    private albumModel: Model<AlbumDocument>,
  ) {}

  @Get()
  getAllAlbums(@Query() query: { artistId: string }) {
    return this.albumModel.find(query);
  }

  @Get(':id')
  getOneAlbum(@Param('id') id: string) {
    return this.albumModel.findById(id);
  }

  @Post()
  @UseInterceptors(
    FileInterceptor('image', { dest: './public/uploads/albums' }),
  )
  createAlbum(
    @UploadedFile() file: Express.Multer.File,
    @Body() albumDto: CreateAlbumDto,
  ) {
    const album = new this.albumModel({
      name: albumDto.name,
      artist: albumDto.artist,
      year: albumDto.year,
      image: file ? '/uploads/albums/' + file.filename : null,
    });

    return album.save();
  }

  @Delete(':id')
  deleteAlbum(@Param('id') id: string) {
    return this.albumModel.deleteOne({ _id: id });
  }
}
